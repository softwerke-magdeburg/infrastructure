package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"goauthentik.io/api"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"slices"
	"strings"
	"time"
)

type VaultUser struct {
	Email string `json:"Email"`
}

func main() {
	testing := false

	authentik_token := os.Getenv("AUTHENTIK_TOKEN")
	authentik_host := os.Getenv("AUTHENTIK_HOST")
	if authentik_token == "" || authentik_host == "" {
		fmt.Fprintf(os.Stderr, "Authentik Host or Token empty!\ntoken: %s\nhost: %s\n", authentik_token, authentik_host)
		os.Exit(2)
	}

	vault_token := os.Getenv("VAULT_ADMIN_TOKEN")
	vault_host := os.Getenv("VAULT_HOST")
	if vault_token == "" || vault_host == "" {
		fmt.Fprintf(os.Stderr, "Vault Host or Token empty!\ntoken: %s\nhost: %s\n", vault_token, vault_host)
		os.Exit(2)
	}

	for {
		authentik_mails := getAllAuthentikUsers(authentik_host, authentik_token)
		vault_mails, vault_client := getAllVaultUsers(vault_token, vault_host)

		notInvitedMails := []string{}
		for _, mail := range authentik_mails {
			if !slices.ContainsFunc(vault_mails, func(s string) bool {
				return strings.ToLower(s) == strings.ToLower(mail)
			}) {
				notInvitedMails = append(notInvitedMails, mail)
				//fmt.Println(mail)
			}
		}

		fmt.Printf("%d not yet invited\n", len(notInvitedMails))

		if !testing {
			// Invite mails
			for _, m := range notInvitedMails {
				resp, _ := vault_client.Post(fmt.Sprintf(
					"https://%s/admin/invite", vault_host),
					"application/json",
					bytes.NewBuffer([]byte(fmt.Sprintf("{\"email\": \"%s\"}", m))))
				defer resp.Body.Close()
				if resp.StatusCode == 200 {
					fmt.Printf("User %s was invited\n", m)
				} else {
					fmt.Fprintf(os.Stderr, "Failed to invite user %s\n", m)
					body, _ := io.ReadAll(resp.Body)
					fmt.Fprintf(os.Stderr, string(body))
				}
			}

		} else {
			fmt.Println("TESTMODE no mails are send!")
		}

		fmt.Println("Sync finished, now I will wait for 6h")
		time.Sleep(1 * time.Hour)
	}
}

// Retrive all Mails for active users
// NOTICE: since pagination does not work, only the first 10000
func getAllAuthentikUsers(host, token string) []string {
	fmt.Println("Retrieve all Mails from Authentik (max 10.000)")
	// Login with Token to Authentik
	conf := api.NewConfiguration()
	conf.Scheme = "https"
	conf.Host = host
	conf.AddDefaultHeader("Authorization", fmt.Sprintf("Bearer %s", token))
	client := api.NewAPIClient(conf)

	//Check Access
	resp, r, err := client.CoreApi.CoreUsersList(context.Background()).IsActive(true).PageSize(10000).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `CoreApi.CoreUsersList``: %v\n", err)
		if r.StatusCode == 403 {
			fmt.Fprintf(os.Stderr, "Please check youre token and if the user can read the userlist\n")
		}
		os.Exit(1)
	}

	//Request all Usermails
	mails := []string{}
	for _, u := range resp.GetResults() {
		//fmt.Printf("%s: %s\n", u.GetName(), u.GetEmail())
		if u.GetEmail() != "" {
			if slices.Contains(mails, u.GetEmail()) {
				fmt.Printf("Mail already read! %s\n", u.GetEmail())
			} else {
				mails = append(mails, u.GetEmail())
			}
		} else {
			fmt.Printf("Account without Mails: %s\n", u.GetName())
		}
	}

	fmt.Printf("Found %d mails\n", len(mails))
	return mails
}

// Retrieve all mails from Bitwarden Admin
func getAllVaultUsers(token, host string) ([]string, http.Client) {
	fmt.Println("Retrieving Mails from Vaultwarden")
	jar, _ := cookiejar.New(nil)
	c := http.Client{Jar: jar}
	resp, err := c.PostForm(fmt.Sprintf("https://%s/admin", host), url.Values{"token": {token}})
	if err != nil {
		fmt.Println()
	}
	fmt.Printf("Vault Login Returncode: %d\n", resp.StatusCode)
	if resp.StatusCode == 401 {
		fmt.Fprintf(os.Stderr, "Your Vault-Token might be wrong\n")
		os.Exit(1)
	}
	if resp.StatusCode == 429 {
		fmt.Fprintf(os.Stderr, "You send too many requests too fast")
		os.Exit(1)
	}
	if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "Vault-Access failed")
		os.Exit(1)
	}

	resp, err = c.Get(fmt.Sprintf("https://%s/admin/users", host))
	fmt.Printf("Vault List Users Returncode: %d\n", resp.StatusCode)
	if resp.StatusCode == 401 {
		fmt.Fprintf(os.Stderr, "Your Vault-Token might be wrong\n")
		os.Exit(1)
	}
	if resp.StatusCode == 429 {
		fmt.Fprintf(os.Stderr, "You send too many requests too fast")
		os.Exit(1)
	}
	if resp.StatusCode != 200 {
		fmt.Fprintf(os.Stderr, "Vault-Access failed")
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	var vaultUsers []VaultUser
	if err := json.Unmarshal(body, &vaultUsers); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse Json from Vault\n")
		os.Exit(1)
	}
	mails := []string{}
	for _, u := range vaultUsers {
		//fmt.Println(u)
		mails = append(mails, strings.ToLower(u.Email))
	}

	fmt.Printf("Found %d mails\n", len(mails))
	return mails, c
}
