module Vault_Authentik_Sync

go 1.21.5

require (
	github.com/golang/protobuf v1.5.4 // indirect
	goauthentik.io/api v1.2021.9 // indirect
	goauthentik.io/api/v3 v3.2024042.13 // indirect
	golang.org/x/net v0.26.0 // indirect
	golang.org/x/oauth2 v0.21.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
)
