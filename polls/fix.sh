#!/bin/sh

# Hide login by email (as we're using Authentik) and links to pricing pages.
grep -rn 'https://support.rallly.co/self-hosting/pricing' /app/apps/web/.next/static/css/ || cat <<EOF | tee -a /app/apps/web/.next/static/css/*.css
a[href="https://support.rallly.co/self-hosting/pricing"],
body>.h-full>.mx-auto.max-w-lg>div>.text-gray-500:last-child,
body>.h-full>.mx-auto.max-w-lg>div>.rounded-lg>:last-child>form>:not(div),
body>.h-full>.mx-auto.max-w-lg>div>.rounded-lg>:last-child>form [type="submit"]
{ display: none; }
EOF

exec ./docker-start.sh "$@"
