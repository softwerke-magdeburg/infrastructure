#!/bin/sh
#
# Calculate days in which a domain expires
# return number of remaining days in influx format

set -eu

if [ $# -ne 1 ]; then
	echo "Usage: $0 <DOMAIN>"
	exit 1
fi

DOMAIN="$1"

TLD=$(echo ${DOMAIN} | awk -F. '{print $NF}')

if [ "${TLD}" == "md" ]; then
	EXPIRES=$(whois -h whois.nic.md ${DOMAIN} | awk '$1 ~ /^Expires/ {print $3}')
elif [ "${TLD}" == "social" ]; then
	EXPIRES=$(whois -h whois.nic.social ${DOMAIN} | awk '/^Registry Expiry Date/ {print $4}' | cut -d T -f 1)
elif [ "${TLD}" == "jetzt" ]; then
	EXPIRES=$(whois -h whois.nic.jetzt ${DOMAIN} | awk '/^Registry Expiry Date/ {print $4}' | cut -d T -f 1)
else
	echo "TLD not supported"
	echo 0
	exit 125
fi

OS=$(uname -s)
if [ "${OS}" == "Linux" ]; then
	DAYS=$(( ($(date +%s -d "${EXPIRES}") - $(date +%s -d "$(date +%Y-%m-%d)")) / 86400))
elif [ "${OS}" == "Darwin" ]; then
	DAYS=$(( ($(date -jf %Y-%m-%d "${EXPIRES}" +%s) - $(date -jf %Y-%m-%d "$(date +%Y-%m-%d)" +%s)) / 86400))
fi

echo "domain_expires,domain=${DOMAIN} days=${DAYS}i"
