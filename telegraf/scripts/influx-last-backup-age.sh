#!/bin/sh
#
# Calculate time since last successful backup ended
# return age in minutes in influx format

set -eu

if [ $# -ne 1 ]; then
	echo "Usage: $0 <FILE>"
	exit 1
fi

STAT_FILE="$1"

AGE=$(( ($(date +%s) - $(tail -n 1 "$STAT_FILE")) / 60 ))

echo "borg_backup v=${AGE}i"
