services:
  element:
    image: "vectorim/element-web:v1.11.94"
    restart: "unless-stopped"
    volumes:
      - "./element-config.json:/app/config.json:ro,Z"
      - "./element-welcome.html:/app/welcome.html:ro,Z"
    labels:
      traefik.enable: "true"
      traefik.http.routers.chat-element.rule: "Host(`${domain_chat:-chat.localhost.mock.directory}`)"
      traefik.http.routers.chat-element.entrypoints: "web"
      traefik.http.routers.chat-element.tls: "true"
      traefik.http.routers.chat-element.middlewares: "hsts"
      traefik.http.services.chat-element.loadbalancer.server.port: "80"

  synapse:
    image: "matrixdotorg/synapse:v1.125.0"
    restart: "unless-stopped"
    entrypoint: "/usr/local/bin/synapse-config-generator.sh"
    depends_on: ["database"]
    healthcheck:
      disable: true  # Wants to access port 8008, which fails for our homeserver.yaml
    environment:
      SYNAPSE_LOG_LEVEL: "WARN"
      SYNAPSE_REPORT_STATS: "no"
      SERVER_NAME: "${synapse_servername}"
      SECRET_KEY: "${synapse_secret_key}"
      PASSWORD_PEPPER: "${synapse_password_pepper}"
      TURN_REALM: "${turn_realm}"
      TURN_SECRET: "${turn_secret}"
      OAUTH_CLIENT_ID: "${oauth_client_id}"
      OAUTH_CLIENT_SECRET: "${oauth_client_secret}"
      DATABASE_PASSWORD: "${database_password}"
      MAIL_SERVER: "${smtp_host}"
      MAIL_USERNAME: "${smtp_user}"
      MAIL_PASSWORD: "${smtp_password}"
      MAIL_FROM: "${smtp_sender}"
    volumes:
      - "/mnt/fast/chat/synapse:/data:Z"
      - "/mnt/slow/chat/synapse-media:/data/media:Z"
      - "./homeserver.yaml:/etc/homeserver-template.yaml:ro,Z"
      - "./synapse-config-generator.sh:/usr/local/bin/synapse-config-generator.sh:ro,Z"
    labels:
      traefik.enable: "true"
      traefik.http.routers.chat.rule: "\
        (\
          (Host(`${domain_chat:-chat.localhost.mock.directory}`) || Host(`${synapse_servername}`) || Host(`elementx-beta.magdeburg.jetzt`))\
          && (PathPrefix(`/_matrix/`) || PathPrefix(`/_synapse/client`))\
          ) ||\
          (Host(`magdeburg.jetzt`) && PathPrefix(`/.well-known/matrix/`))\
        "
      traefik.http.routers.chat.entrypoints: "web"
      traefik.http.routers.chat.tls: "true"
      traefik.http.routers.chat.middlewares: "hsts, chat-synapse-headers"
      traefik.http.services.chat-synapse.loadbalancer.server.port: "8080"
      traefik.http.middlewares.chat-synapse-headers.headers.customrequestheaders.X-Forwarded-Proto: "https"

  database:
    image: "postgres:17.4"
    restart: "unless-stopped"
    stop_grace_period: "1800s"
    volumes:
      - "/mnt/fast/chat/database:/var/lib/postgresql/data:Z"
    environment:
      POSTGRES_DB: "synapse"
      POSTGRES_PASSWORD: "${database_password}"
      POSTGRES_INITDB_ARGS: "--encoding=UTF-8 --lc-collate=C --lc-ctype=C"

  # TODO: Test if it works, add a certificate & add a firewall rule in technik-setup
  coturn:
    image: "ghcr.io/coturn/coturn:4.6"
    restart: "unless-stopped"
    network_mode: "host"
    command: [
      "-n", "--log-file=stdout",
      "--listening-port=143", "--tls-listening-port=993",  # Using IMAP ports as that works in most firewalls.
      "--listening-ip=${turn_ipv4_address}",
      # "--listening-ip=${turn_ipv6_address}",  # TODO: IPv6
      "--external-ip=${turn_ipv4_address}",
      # "--external-ip=${turn_ipv6_address}",  # TODO: IPv6
      "--use-auth-secret", "--static-auth-secret=${turn_secret}",
      "--realm=${turn_realm}",
      "--no-cli", "--no-tcp-relay",
      "--user-quota=12", "--total-quota=1200",
      # "--cert=/etc/ssl/private/turn.pem", "--pkey=/etc/ssl/private/turn.key"  # TODO: TLS-Zertifikate (eigener Let's Encrypt Service oder Extraktion aus Traefik?!)
      "--denied-peer-ip=10.0.0.0-10.255.255.255", "--denied-peer-ip=192.168.0.0-192.168.255.255", "--denied-peer-ip=172.16.0.0-172.31.255.255",
      "--no-multicast-peers",
      "--denied-peer-ip=0.0.0.0-0.255.255.255", "--denied-peer-ip=100.64.0.0-100.127.255.255", "--denied-peer-ip=127.0.0.0-127.255.255.255", "--denied-peer-ip=169.254.0.0-169.254.255.255", "--denied-peer-ip=192.0.0.0-192.0.0.255", "--denied-peer-ip=192.0.2.0-192.0.2.255", "--denied-peer-ip=192.88.99.0-192.88.99.255", "--denied-peer-ip=198.18.0.0-198.19.255.255", "--denied-peer-ip=198.51.100.0-198.51.100.255", "--denied-peer-ip=203.0.113.0-203.0.113.255", "--denied-peer-ip=240.0.0.0-255.255.255.255",
    ]

  well-known:
    image: nginx:1.27-alpine
    restart: unless-stopped
    volumes:
      - "/mnt/fast/chat/well-known/:/usr/share/nginx/html:Z:ro"
    labels:
      traefik.enable: "true"
      traefik.http.routers.chat-well-known.rule: "PathPrefix(`/.well-known/matrix/`)"
      traefik.http.routers.chat-well-known.entrypoints: "web"
      traefik.http.routers.chat-well-known.tls: "true"
      traefik.http.routers.chat-well-known.middlewares: "hsts"
      traefik.http.routers.chat-well-known.priority: 1000
      traefik.http.services.chat-well-known.loadbalancer.server.port: "80"

  # The following setion lets users set "elementx-beta.magdeburg.jetzt" as a homeserver address in ElementX to get the sliding window sync.

  elementx-beta-well-known:
    image: nginx:1.27-alpine
    restart: unless-stopped
    volumes:
      - "/mnt/fast/chat/elementx-beta-well-known/:/usr/share/nginx/html:Z:ro"
    labels:
      traefik.enable: "true"
      traefik.http.routers.chat-elementx-beta-well-known.rule: "Host(`elementx-beta.magdeburg.jetzt`) && PathPrefix(`/.well-known/matrix/`)"
      traefik.http.routers.chat-elementx-beta-well-known.entrypoints: "web"
      traefik.http.routers.chat-elementx-beta-well-known.tls: "true"
      traefik.http.routers.chat-elementx-beta-well-known.middlewares: "hsts"
      traefik.http.routers.chat-elementx-beta-well-known.priority: 2000
      traefik.http.services.chat-elementx-beta-well-known.loadbalancer.server.port: "80"

  elementx-beta-database:
    image: "postgres:17.4"
    restart: "unless-stopped"
    stop_grace_period: "1800s"
    volumes:
      - "/mnt/fast/chat/elementx-beta-database:/var/lib/postgresql/data:Z"
    environment:
      POSTGRES_DB: "slidingsync"
      POSTGRES_PASSWORD: "slidingsync"  # TODO: set a secure password
      POSTGRES_INITDB_ARGS: "--encoding=UTF-8 --lc-collate=C --lc-ctype=C"

  elementx-beta-sliding-sync:
    image: ghcr.io/matrix-org/sliding-sync:latest
    restart: unless-stopped
    environment:
      SYNCV3_SERVER: https://chat.magdeburg.jetzt
      SYNCV3_DB: postgresql://postgres:slidingsync@elementx-beta-database/slidingsync?sslmode=disable
      SYNCV3_SECRET: O18igtXDtP2AtJ4cIlv4Xx7n70rBjsKcaboanpKtI3JAJR385lkrLJkCtEJ1VDZg
    labels:
      traefik.enable: "true"
      traefik.http.routers.chat-elementx-beta-sliding-sync.rule: "Host(`elementx-beta.magdeburg.jetzt`)"
      traefik.http.routers.chat-elementx-beta-sliding-sync.entrypoints: "web"
      traefik.http.routers.chat-elementx-beta-sliding-sync.tls: "true"
      traefik.http.routers.chat-elementx-beta-sliding-sync.middlewares: "hsts"
      traefik.http.services.chat-elementx-beta-sliding-sync.loadbalancer.server.port: "8008"

  compress-states:
    image: softwerke/synapse-compress-state
    build: "./rust-synapse-compress-state-docker"
    environment:
      POSTGRES_USER: "postgres"
      POSTGRES_PASSWORD: "${database_password}"
      POSTGRES_HOST: database
      POSTGRES_DB: synapse
      CHUNKS_TO_COMPRESS: 10000000
      CHUNK_SIZE: 1000
